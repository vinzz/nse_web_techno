author = "Dr Claw"
license = "Same as Nmap--See https://nmap.org/book/man-legal.html"
categories = {"discovery"}

-- @output
-- 80/tcp   open  http
-- | web_techno:
-- |_Redirection to: https://github.com
-- 443/tcp  open  https
-- | web_techno:
-- | "[UI frameworks] Bootstrap:46"
-- | "[PaaS] GitHub Pages:null"
-- | "[Web frameworks] Ruby on Rails:null"
-- |_"[Programming languages] Ruby:null"

description = [[
  Find web technology base on wappalyzer.
]]

local nmap = require "nmap"
local stdnse = require "stdnse"
local http = require("socket.http")
local https = require("ssl.https")
local shortport = require "shortport"

portrule = function(host, port)
  if port.service == "https" or port.service == "http" then
    return port.protocol == "tcp"
      and port.state == "open"
  end
end

action = function(host, port)
    if port.service == "http" then 
      url = "http://" .. host.targetname .. ":" .. port.number .. "/"
      body, code, headers, status = http.request(url)
    else 
      url = "https://" .. host.targetname .. ":" .. port.number .. "/"
      body, code, headers, status = https.request(url)
    end
    if tonumber(code) > 400 then
      err_output = status
      for key, value in pairs(headers) do
        err_output = err_output .. key .. value .."\n"
      end
      output = "Error:\n" .. code .. "\n" .. err_output
      return output
    elseif tonumber(code) == 301 then
      url = headers.location
    end
    local cmd = "wappalyzer " .. url .. " 2> /dev/null | jq '.applications[] | \"[\\(.categories[] | .[])] \\(.name):\\(.version)\"'"
    output = "\n" .. io.popen(cmd, "r"):read("*a"):sub(1, -2) 
    if output == '\n' then
      if headers.location ~= '' or headers.location ~= nil then
        output = "\nRedirection to: " .. headers.location
      else 
        output = "No data"
      end
    end
    return output
  end
